-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: project_nandemo
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `project_nandemo`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `project_nandemo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `project_nandemo`;

--
-- Table structure for table `Pair`
--

DROP TABLE IF EXISTS `Pair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quizId` int(11) NOT NULL,
  `pairA` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pairB` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quizId` (`quizId`,`pairA`,`pairB`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pair`
--

LOCK TABLES `Pair` WRITE;
/*!40000 ALTER TABLE `Pair` DISABLE KEYS */;
INSERT INTO `Pair` VALUES (5,1,'Blue','#0000FF'),(4,1,'Green','#00FF00'),(6,1,'Indigo','#2E2B5F'),(2,1,'Orange','#FF7F00'),(1,1,'Red','#FF0000'),(7,1,'Violet','#8B00FF'),(3,1,'Yellow','#FFFF00'),(13,2,'My birthday','2005-10-25'),(9,2,'My favorite cake','Chocolate mousse cake'),(8,2,'My favorite food','Rose pasta'),(10,2,'My favorite fruit','Strawberry'),(11,2,'My favorite juice','Shake shack milkshake'),(12,2,'My favorite singer','Sekai no owari'),(14,2,'Why I hate you','I don\'t know'),(15,3,'How many wives did Henry VIII have beheaded?','Two'),(16,3,'In Egypt, what are the mummy\'s coffins placed insi','A sarcophagus'),(19,3,'In World War II, who wrote in her diary about hidi','Anne Frank'),(18,3,'In which year did World War II begin?','1939'),(17,3,'What are the Mayans famous for building?','Pyramids'),(21,3,'What did the Egyptians use to make paper?','Papyrus'),(22,3,'What does BCE mean?','Before Common Era'),(20,3,'Which year was the World Wide Web launched?','1990'),(44,4,'ก๋วยเตี๋ยว','Rice noodles'),(40,4,'ข้าวต้ม','Khao tom'),(39,4,'ข้าวผัด','Khao phat'),(43,4,'ต้มยำกุ้ง','Tom yum goong'),(42,4,'ผัดไทย','Pad thai'),(41,4,'ส้มตำ','Somtam'),(46,4,'ไก่ทอด','Kai todd'),(45,4,'ไก่ย่าง','Kai yang');
/*!40000 ALTER TABLE `Pair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlayLog`
--

DROP TABLE IF EXISTS `PlayLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlayLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerId` int(11) NOT NULL,
  `quizId` int(11) NOT NULL,
  `winLose` tinyint(1) NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlayLog`
--

LOCK TABLES `PlayLog` WRITE;
/*!40000 ALTER TABLE `PlayLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `PlayLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Player`
--

DROP TABLE IF EXISTS `Player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `teamId` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Player`
--

LOCK TABLES `Player` WRITE;
/*!40000 ALTER TABLE `Player` DISABLE KEYS */;
INSERT INTO `Player` VALUES (1,'nandemo','$2a$10$roZ67fCJsPVTfFuQyO3fh023hQyoweauLY5VU6Rlg.3jE4e2VlC90LrGBn0NezG',1,1),(2,'imouto','$2a$10$roZ67fCJsPVTfFuQyO3fh023hQyoweauLY5VU6Rlg.3jE4e2VlC90LrGBn0NezG',1,2),(3,'sensei','$2a$10$roZ67fCJsPVTfFuQyO3fh023hQyoweauLY5VU6Rlg.3jE4e2VlC90LrGBn0NezG',1,3),(4,'sagat','$2a$10$roZ67fCJsPVTfFuQyO3fh023hQyoweauLY5VU6Rlg.3jE4e2VlC90LrGBn0NezG',1,3);
/*!40000 ALTER TABLE `Player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Quiz`
--

DROP TABLE IF EXISTS `Quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerId` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `playerId` (`playerId`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Quiz`
--

LOCK TABLES `Quiz` WRITE;
/*!40000 ALTER TABLE `Quiz` DISABLE KEYS */;
INSERT INTO `Quiz` VALUES (1,1,'Color'),(2,2,'What I like'),(3,3,'History'),(4,4,'Thai food');
/*!40000 ALTER TABLE `Quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Team`
--

LOCK TABLES `Team` WRITE;
/*!40000 ALTER TABLE `Team` DISABLE KEYS */;
INSERT INTO `Team` VALUES (3,'Boss'),(2,'Imouto'),(1,'Nandemo');
/*!40000 ALTER TABLE `Team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'project_nandemo'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-01 14:26:10