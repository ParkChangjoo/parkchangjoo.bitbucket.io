package com.pcj.project_nandemo.controller.game;

import com.pcj.project_nandemo.model.dto.game.GameDataRoom;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.service.OnlineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Slf4j
@Controller
@Transactional
@RequiredArgsConstructor
public class GameController {

    private final OnlineService onlineService;

    private final PlayerRepository playerRepository;

    @GetMapping("/game/online")
    public String online(Model model, Principal principal) {
        Player player = playerRepository.findByName(principal.getName());

        model.addAttribute("myTeamId", player.getTeam().getId());
        model.addAttribute("myPlayerId", player.getId());

        return "game/online";
    }

    @MessageMapping("/topic/game/online")
    public void onlineMessage(GameDataRoom room) {
        onlineService.UpdateRoomGameData(room);

        if (room.getGame_status() != GameDataRoom.GameStatus.finished)
            onlineService.SendMessage(room.getId());
    }
}