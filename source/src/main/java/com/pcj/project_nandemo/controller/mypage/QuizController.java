package com.pcj.project_nandemo.controller.mypage;

import com.pcj.project_nandemo.model.dto.mypage.MyQuizCreateDto;
import com.pcj.project_nandemo.model.dto.mypage.MyQuizDeleteDto;
import com.pcj.project_nandemo.model.dto.mypage.MyQuizEditDto;
import com.pcj.project_nandemo.model.entity.Pair;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.model.entity.Quiz;
import com.pcj.project_nandemo.repository.PairRepository;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.repository.QuizRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@Transactional
@RequiredArgsConstructor
public class QuizController {

    private final PlayerRepository playerRepository;

    private final QuizRepository quizRepository;

    private final PairRepository pairRepository;

    @GetMapping("/myPage/myQuiz")
    public String myQuiz(Model model, Principal principal)
    {
        int playerId = playerRepository.findByName(principal.getName()).getId();

        List<Quiz> quizeList = quizRepository.findByPlayerId(playerId);
        model.addAttribute("quizeList", quizeList);

        return "myPage/myQuiz";
    }

    @GetMapping("/myPage/myQuizCreate")
    public String myQuizCreateGet(MyQuizCreateDto myQuizCreateDto)
    {
        return "myPage/myQuizCreate";
    }

    @PostMapping("/myPage/myQuizCreate")
    public String myQuizCreatePost(@Valid MyQuizCreateDto myQuizCreateDto, BindingResult bindingResult, Principal principal)
    {
        if (bindingResult.hasErrors()) {
            return "myPage/myQuizCreate";
        }
        else {
            // items validation
            for (String pair : myQuizCreateDto.getPairList().split("\r\n\r\n"))
            {
                if (pair.split("\r\n").length != 2) {
                    bindingResult.addError(new FieldError("myQuizCreateDto", "pairList",myQuizCreateDto.getPairList(), false, null, null, "Quiz pairs are not valid"));
                    return "myPage/myQuizCreate";
                }
            }

            Player player = playerRepository.findByName(principal.getName());

            Quiz quiz = new Quiz();
            quiz.setPlayer(player);
            quiz.setName(myQuizCreateDto.getName());
            quizRepository.save(quiz);
            quizRepository.flush();

            List<Pair> pairList = new ArrayList<>();
            for (String pair : myQuizCreateDto.getPairList().split("\r\n\r\n"))
            {
                String[] pairAB = pair.split("\r\n");
                String pairA = pairAB[0];
                String pairB = pairAB[1];

                Pair pairEntity = new Pair();
                pairEntity.setQuiz(quiz);
                pairEntity.setPairA(pairA);
                pairEntity.setPairB(pairB);

                pairList.add(pairEntity);
            }
            pairRepository.saveAll(pairList);

            return "redirect:/myPage/myQuiz";
        }
    }

    @GetMapping("/myPage/myQuizEdit/{id}")
    public String myQuizEditGet(@PathVariable(required = true) int id, MyQuizEditDto myQuizEditDto)
    {
        Quiz quiz = quizRepository.findById(id).get();
        List<Pair> pairList = pairRepository.findByQuizId(quiz.getId());

        String pairsCsv = "";
        for (Pair pair : pairList)
        {
            String line = pair.getPairA() + "\r\n" + pair.getPairB();
            pairsCsv += line;
            pairsCsv += "\r\n\r\n";
        }

        myQuizEditDto.setId(quiz.getId());
        myQuizEditDto.setName(quiz.getName());
        myQuizEditDto.setPairList(pairsCsv);

        return "myPage/myQuizEdit";
    }

    @PostMapping("/myPage/myQuizEdit")
    public String myQuizEditPost(@Valid MyQuizEditDto myQuizEditDto, BindingResult bindingResult){

        if (bindingResult.hasErrors()) {
            return "myPage/myQuizEdit";
        }
        else {
            // items validation
            for (String item : myQuizEditDto.getPairList().split("\r\n\r\n"))
            {
                if (item.split("\r\n").length != 2) {
                    bindingResult.addError(new FieldError("myQuizEditDto", "pairList",myQuizEditDto.getPairList(), false, null, null, "Quiz pairs are not valid"));
                    return "myPage/myQuizEdit";
                }
            }

            Quiz quiz = quizRepository.findById(myQuizEditDto.getId()).get();
            quiz.setName(myQuizEditDto.getName());
            quizRepository.save(quiz);

            pairRepository.deleteByQuizId(myQuizEditDto.getId());
            pairRepository.flush();

            List<Pair> pairList = new ArrayList<>();
            for (String pair : myQuizEditDto.getPairList().split("\r\n\r\n"))
            {
                String[] pairAB = pair.split("\r\n");
                String pairA = pairAB[0];
                String pairB = pairAB[1];

                Pair pairEntity = new Pair();
                pairEntity.setQuiz(quiz);
                pairEntity.setPairA(pairA);
                pairEntity.setPairB(pairB);

                pairList.add(pairEntity);
            }
            pairRepository.saveAll(pairList);

            return "redirect:/myPage/myQuiz";
        }
    }

    @GetMapping("/myPage/myQuizDelete/{id}")
    public String myQuizDeleteGet(@PathVariable(required = true) int id, MyQuizDeleteDto myQuizDeleteDto)
    {
        Quiz quiz = quizRepository.findById(id).get();
        myQuizDeleteDto.setId(quiz.getId());
        myQuizDeleteDto.setName(quiz.getName());

        return "myPage/myQuizDelete";
    }

    @PostMapping("/myPage/myQuizDelete")
    public String myQuizDeletePost(@Valid MyQuizDeleteDto myQuizDeleteDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "myPage/myQuizDelete";
        }
        else {
            pairRepository.deleteByQuizId(myQuizDeleteDto.getId());
            quizRepository.deleteById(myQuizDeleteDto.getId());
            return "redirect:/myPage/myQuiz";
        }
    }
}