package com.pcj.project_nandemo.controller.mypage;

import com.pcj.project_nandemo.model.dto.mypage.MyTeamChangeDto;
import com.pcj.project_nandemo.model.dto.mypage.MyTeamCreateDto;
import com.pcj.project_nandemo.model.dto.mypage.TeamDto;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.model.entity.Team;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@Transactional
@RequiredArgsConstructor
public class TeamController {

    private final PlayerRepository playerRepository;

    private final TeamRepository teamRepository;

    @GetMapping("/myPage/myTeam")
    public String myTeam(Model model, Principal principal)
    {
        Player player = playerRepository.findByName(principal.getName());
        Team team = player.getTeam();

        model.addAttribute("team", team);

        return "myPage/myTeam";
    }

    @GetMapping("/myPage/myTeamCreate")
    public String myTeamCreateGet(MyTeamCreateDto myTeamCreateDto)
    {
        return "myPage/myTeamCreate";
    }

    @PostMapping("/myPage/myTeamCreate")
    public String myTeamCreatePost(@Valid MyTeamCreateDto myTeamCreateDto, BindingResult bindingResult, Principal principal)
    {
        if (bindingResult.hasErrors()) {
            return "myPage/myTeamCreate";
        }
        else if (teamRepository.findByName(myTeamCreateDto.getName()) != null) {
            bindingResult.addError(new FieldError("myTeamCreateDto", "name",myTeamCreateDto.getName(), false, null, null, "Team name already exists"));
            return "myPage/myTeamCreate";
        }
        else {
            Player player = playerRepository.findByName(principal.getName());
            Team beforeTeam = player.getTeam();

            Team team = new Team();
            team.setName(myTeamCreateDto.getName());
            teamRepository.save(team);
            teamRepository.flush();

            player.setTeam(team);
            playerRepository.save(player);
            playerRepository.flush();

            // if old team has no member, remove team
            List<Player> beforeTeamUserList = playerRepository.findByTeamId(beforeTeam.getId());
            if (beforeTeamUserList.isEmpty()) {
                teamRepository.delete(beforeTeam);
            }

            return "redirect:/myPage/myTeam";
        }
    }

    @GetMapping("/myPage/myTeamChange")
    public String myTeamChangeGet(MyTeamChangeDto myTeamChangeDto, Principal principal)
    {
        Player player = playerRepository.findByName(principal.getName());

        List<Team> teamList = teamRepository.findAll().stream()
                .filter(t -> !t.getPlayerList().contains(player)).collect(Collectors.toList());

        List<TeamDto> teamDtoList = new ArrayList<>();
        teamList.forEach(team -> {
            TeamDto teamDto = new TeamDto();
            teamDto.setId(team.getId());
            teamDto.setName(team.getName());
            teamDtoList.add(teamDto);
        });

        if (!teamList.isEmpty()) {
            myTeamChangeDto.setTeamList(teamDtoList);
            myTeamChangeDto.setSelectedTeamId(teamList.get(0).getId());
        }

        return "myPage/myTeamChange";
    }

    @PostMapping("/myPage/myTeamChange")
    public String myTeamChangePost(@Valid MyTeamChangeDto myTeamChangeDto, BindingResult bindingResult, Principal principal)
    {
        if (bindingResult.hasErrors()) {
            return "myPage/myTeamChange";
        }
        else {
            Player player = playerRepository.findByName(principal.getName());
            Team beforeTeam = player.getTeam();

            player.setTeam(teamRepository.findById(myTeamChangeDto.getSelectedTeamId()).get());
            playerRepository.save(player);
            playerRepository.flush();

            // if old team has no member, remove team
            List<Player> beforeTeamUserList = playerRepository.findByTeamId(beforeTeam.getId());
            if (beforeTeamUserList.isEmpty()) {
                teamRepository.delete(beforeTeam);
            }

            return "redirect:/myPage/myTeam";
        }
    }
}