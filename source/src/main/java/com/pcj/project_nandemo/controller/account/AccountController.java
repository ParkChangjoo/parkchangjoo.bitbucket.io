package com.pcj.project_nandemo.controller.account;

import com.pcj.project_nandemo.model.dto.account.PasswdChangeDto;
import com.pcj.project_nandemo.model.dto.account.RegisterDto;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;

@Slf4j
@Controller
@RequiredArgsConstructor
public class AccountController {
    private final PlayerRepository playerRepository;

    private final TeamRepository teamRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    @GetMapping("/403")
    public String Status403() {
        throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Unauthorized");
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/logout")
    public String logout() {
        return "logout";
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/myPage/passwdChange")
    public String passwdChangeGet(PasswdChangeDto passwdChangeDto, Principal principal)
    {
        return "myPage/passwdChange";
    }

    @PostMapping("/myPage/passwdChange")
    public String passwdChangePost(@Valid PasswdChangeDto passwdChangeDto, BindingResult bindingResult, Principal principal)
    {
        if (bindingResult.hasErrors()) {
            return "myPage/passwdChange";
        }
        else if (!passwdChangeDto.getPassword().equals(passwdChangeDto.getConfirmPassword())) {
            bindingResult.addError(new FieldError("passwdChangeDto", "confirmPassword", passwdChangeDto.getConfirmPassword(), false, null, null, "Confirm password does not match"));
            return "myPage/passwdChange";
        }
        else {
            Player player = playerRepository.findByName(principal.getName());
            player.setPassword(passwordEncoder.encode(passwdChangeDto.getPassword()));
            playerRepository.save(player);

            return "redirect:/myPage/passwdChangeComplete";
        }
    }

    @GetMapping("//myPage/passwdChangeComplete")
    public String passwdChangeComplete() { return "myPage/passwdChangeComplete"; }

    @GetMapping("/register")
    public String registerGet(RegisterDto registerDto) {
        return "register";
    }

    @PostMapping("/register")
    public String registerPost(@Valid RegisterDto registerDto, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "register";
        }
        else if (playerRepository.findByName(registerDto.getName()) != null) {
            bindingResult.addError(new FieldError("registerDto", "name", registerDto.getName(), false, null, null, "Name already exists"));
            return "register";
        }
        else if (!registerDto.getPassword().equals(registerDto.getConfirmPassword())) {
            bindingResult.addError(new FieldError("registerDto", "confirmPassword", registerDto.getConfirmPassword(), false, null, null, "Confirm password does not match"));
            return "register";
        }
        else {
            Player player = new Player();
            player.setName(registerDto.getName());
            player.setPassword(passwordEncoder.encode(registerDto.getPassword()));
            player.setLevel(1);
            player.setTeam(teamRepository.findById(1).get());
            playerRepository.save(player);

            return "redirect:registerComplete";
        }
    }

    @GetMapping("/registerComplete")
    public String registerComplete() { return "registerComplete"; }
}