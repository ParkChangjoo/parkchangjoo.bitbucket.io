package com.pcj.project_nandemo.controller.game;

import com.pcj.project_nandemo.model.entity.PlayLog;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.model.entity.Quiz;
import com.pcj.project_nandemo.repository.PlayLogRepository;
import com.pcj.project_nandemo.repository.QuizRepository;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.model.dto.game.*;
import com.pcj.project_nandemo.service.OnlineService;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RestController
@RequiredArgsConstructor
public class GameApiController {

    private final PlayerRepository playerRepository;

    private final QuizRepository quizRepository;

    private final PlayLogRepository playLogRepository;

    private final OnlineService onlineService;

    @GetMapping(value = "/game/quizList")
    public List<QuizDto> quizListGet(Principal principal) {

        List<QuizDto> quizDtoList = new ArrayList<>();

        List<Quiz> quizList = quizRepository.findAll();
        quizList.forEach(quiz -> {
            QuizDto quizDto = new QuizDto();
            quizDto.setId(quiz.getId());
            quizDto.setName(quiz.getName());
            quizDto.setPlayerName(quiz.getPlayer().getName());
            quizDtoList.add(quizDto);
        });

        return quizDtoList;
    }

    @GetMapping(value = "/game/roomList")
    public List<RoomDto> roomListGet(@RequestParam(name="quizId", required = true) int quizId, Principal principal) {

        Player player = playerRepository.findByName(principal.getName());
        List<RoomDto> roomDtoList = onlineService.roomList(quizId, player.getId());
        return roomDtoList;
    }

    @PostMapping(value = "/game/joinRoom")
    public GameDataRoom joinRoomPost(@Valid @RequestBody RoomJoinDto roomJoinDto, Principal principal) {

        // 1. get room
        Player player = playerRepository.findByName(principal.getName());
        GameDataRoom room = onlineService.JoinRoom(roomJoinDto.getRoomId(), player.getId());

        // 2. return room
        return room;
    }

    @PostMapping(value = "/game/createRoom")
    public GameDataRoom createRoomPost(@Valid @RequestBody RoomCreateDto roomCreateDto, Principal principal) {

        // 1. create room
        Player player = playerRepository.findByName(principal.getName());
        GameDataRoom room = onlineService.CreateRoom(roomCreateDto.getRoomName(), roomCreateDto.getQuizId(), player.getId());

        // 2. return room
        return room;
    }

    @PostMapping(value = "/game/playLogCreate")
    public void playLogCreate(@Valid @RequestBody PlayLogDto playLogDto, Principal principal) {

        Player player = playerRepository.findByName(principal.getName());

        if (playLogDto.getWinLose() == true)
            player.setLevel(player.getLevel() + 1);

        PlayLog playLog = new PlayLog();

        playLog.setPlayer(player);
        playLog.setQuiz(quizRepository.findById(playLogDto.getQuizId()).get());
        playLog.setWinLose(playLogDto.getWinLose());

        playLogRepository.save(playLog);
    }
}