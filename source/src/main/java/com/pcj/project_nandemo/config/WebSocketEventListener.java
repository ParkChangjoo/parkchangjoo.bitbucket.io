package com.pcj.project_nandemo.config;

import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.service.OnlineService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Configuration
@RequiredArgsConstructor
public class WebSocketEventListener {

    private  final PlayerRepository playerRepository;
    private final OnlineService onlineService;

    @EventListener
    private void handleSessionConnected(SessionConnectEvent event) {
        // do nothing
    }

    @EventListener
    private void handleSessionDisconnected(SessionDisconnectEvent event) {
        Player player = playerRepository.findByName(event.getUser().getName());
        onlineService.DeleteRoom(player.getId());
    }
}
