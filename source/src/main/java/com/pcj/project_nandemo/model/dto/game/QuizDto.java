package com.pcj.project_nandemo.model.dto.game;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuizDto {

    private int id;

    private String name;

    private String playerName;
}