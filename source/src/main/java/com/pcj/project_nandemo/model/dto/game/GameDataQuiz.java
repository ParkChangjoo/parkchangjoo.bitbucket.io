package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GameDataQuiz implements Serializable {

    private int id;

    private String name;

    private String playerName;

    private List<GameDataPair> pairList;

    public GameDataQuiz() {
        this.pairList = new ArrayList<>();
    }
}
