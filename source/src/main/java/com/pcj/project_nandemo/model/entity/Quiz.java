package com.pcj.project_nandemo.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Quiz {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="playerId")
    private Player player;

    private String name;

    @OneToMany(mappedBy = "quiz")
    private List<Pair> pairList;

    @OneToMany(mappedBy = "quiz")
    private List<PlayLog> playLogList;
}