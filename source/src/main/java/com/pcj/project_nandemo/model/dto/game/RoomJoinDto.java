package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RoomJoinDto {

    @NotNull(message = "Room id is required")
    private String roomId;
}