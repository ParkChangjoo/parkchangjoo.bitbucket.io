package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GameDataTeam implements Serializable {

    private int id;

    private String name;

    private int score;

    private List<GameDataPlayer> playerList;

    public GameDataTeam() {
        this.score = 0;
        this.playerList = new ArrayList<>();
    }
}
