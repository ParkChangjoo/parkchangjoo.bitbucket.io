package com.pcj.project_nandemo.model.dto.mypage;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class MyTeamCreateDto {

    @NotEmpty(message = "Team name is required")
    @Size(max=50, message = "Team name must be less than 50 characters")
    private String name;
}