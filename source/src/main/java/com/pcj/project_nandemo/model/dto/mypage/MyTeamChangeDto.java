package com.pcj.project_nandemo.model.dto.mypage;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class MyTeamChangeDto {

    @NotNull(message = "Team must be selected")
    private int selectedTeamId;

    private List<TeamDto> teamList;
}