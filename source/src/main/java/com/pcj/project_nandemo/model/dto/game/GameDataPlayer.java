package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GameDataPlayer implements Serializable {

    private int id;

    private String name;

    private int score;

    private boolean ready;

    public GameDataPlayer() {
        this.score = 0;
        this.ready = false;
    }
}