package com.pcj.project_nandemo.model.dto.mypage;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyQuizDeleteDto {

    @NotNull(message = "Quiz id is required")
    private int id;

    private String name;
}