package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomDto {

    private String id;

    private String name;
}