package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class GameDataRoom implements Serializable {
    public enum GameStatus {
        not_started, started, finished
    }

    // [Room meta data] Room id
    private final String id = UUID.randomUUID().toString();

    // [Room meta data] Room name
    private String name;

    // [Room meta data] Game status
    private GameStatus game_status;

    // [Room meta data] Game players
    private List<Integer> player_list;

    // [Room game data] Game quiz
    private GameDataQuiz quiz;

    // [Room game data] Game teams
    private List<GameDataTeam> team_list;

    public GameDataRoom() {
        this.game_status = GameStatus.not_started;
        this.player_list = new ArrayList<>();
        this.quiz = new GameDataQuiz();
        this.team_list = new ArrayList<>();
    }
}
