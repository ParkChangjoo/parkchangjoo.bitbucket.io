package com.pcj.project_nandemo.model.dto.mypage;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class MyQuizEditDto {

    @NotNull(message = "Quiz id is required")
    private int id;

    @NotEmpty(message = "Quiz name is required")
    @Size(max=50, message = "Quiz name must be less than 50 characters")
    private String name;

    @NotEmpty(message = "Quiz pairs are required")
    private String pairList;
}