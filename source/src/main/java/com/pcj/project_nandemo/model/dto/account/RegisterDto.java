package com.pcj.project_nandemo.model.dto.account;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RegisterDto {

    @NotEmpty(message = "Name is required")
    @Size(max=50, message = "Name is too long")
    private String name;

    @NotEmpty(message = "Password is required")
    @Size(min=4, message = "Password must be at least 4 characters")
    @Size(max=50, message = "Password is too long")
    private String password;

    @NotEmpty(message = "Confirm password is required")
    @Size(min=4, message = "Confirm password must be at least 4 characters")
    @Size(max=50, message = "Confirm password is too long")
    private String confirmPassword;
}