package com.pcj.project_nandemo.model.dto.mypage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamDto {

    private int id;

    private String name;
}