package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GameDataPair implements Serializable {

    private int id;

    private String type;

    private String text;

    private boolean disabled;

    private float opacity;

    private int team_id;

    private int player_id;

    public GameDataPair() {
        this.disabled = true;
        this.opacity = 1;
        this.team_id = -1;
        this.player_id = -1;
    }
}