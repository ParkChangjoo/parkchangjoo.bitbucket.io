package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PlayLogDto {

    @NotNull(message = "Quiz id is required")
    private int quizId;

    @NotNull(message = "WinLose is required")
    private Boolean winLose;
}