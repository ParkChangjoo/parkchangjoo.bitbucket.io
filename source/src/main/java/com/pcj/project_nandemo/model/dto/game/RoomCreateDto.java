package com.pcj.project_nandemo.model.dto.game;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RoomCreateDto {

    @NotNull(message = "Room name is required")
    private String roomName;

    @NotNull(message = "Quiz id is required")
    private int quizId;
}