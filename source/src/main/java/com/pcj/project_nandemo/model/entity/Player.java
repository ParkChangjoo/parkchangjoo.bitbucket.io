package com.pcj.project_nandemo.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Player {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String name;

    private String password;

    private int level;

    @ManyToOne
    @JoinColumn(name="teamId")
    private Team team;

    @OneToMany(mappedBy = "player")
    private List<Quiz> quizList;

    /*

    @JsonManagedReference
    @OneToMany(mappedBy = "user")
    private List<Quiz> quizList;

    @JsonManagedReference
    @OneToMany(mappedBy = "user")
    private List<PlayLog> playLogList;*/
}