package com.pcj.project_nandemo.service;

import com.pcj.project_nandemo.model.dto.game.GameDataRoom;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OnlinePublisher {

    private final RedisTemplate<String, Object> redisTemplate;

    public void Publish(ChannelTopic topic, GameDataRoom room) {
        redisTemplate.convertAndSend(topic.getTopic(), room);
    }
}
