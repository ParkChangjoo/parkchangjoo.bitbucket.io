package com.pcj.project_nandemo.service;

import com.pcj.project_nandemo.model.dto.game.*;
import com.pcj.project_nandemo.model.entity.Player;
import com.pcj.project_nandemo.model.entity.Quiz;
import com.pcj.project_nandemo.model.entity.Team;
import com.pcj.project_nandemo.repository.PlayerRepository;
import com.pcj.project_nandemo.repository.QuizRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class OnlineService {
    private static final String GAME_ROOM_HASH_KEY = "GAME_ROOM";
    private HashOperations<String, String, GameDataRoom> gameRoomHash; // <roomId, gameData>
    
    private static final String GAME_ROOM_USER_HASH_KEY = "GAME_ROOM_USER";
    private HashOperations<String, Integer, String> gameRoomUserHash; // <playerId, roomId>
    
    private Map<String, ChannelTopic> gameRoomTopics;

    private final PlayerRepository playerRepository;

    private final QuizRepository quizRepository;

    private final RedisTemplate<String, Object> redisTemplate;

    private final OnlinePublisher onlinePublisher;

    private final RedisMessageListenerContainer redisMessageListenerContainer;

    private final OnlineSubscriber onlineSubscriber;

    @PostConstruct
    private void Initialize() {
        gameRoomHash = redisTemplate.opsForHash();
        gameRoomUserHash = redisTemplate.opsForHash();

        gameRoomTopics = new HashMap<>();
    }

    public List<RoomDto> roomList(int quizId, int playerId) {

        // Check with 1) playerId 2) quid id 3) game status 4) count of players (max 4)
        Map<String, GameDataRoom> playableRoomMap = gameRoomHash.entries(GAME_ROOM_HASH_KEY).entrySet().stream()
                .filter(el -> !el.getValue().getPlayer_list().contains(playerId) &&
                        el.getValue().getQuiz().getId() == quizId &&
                        el.getValue().getGame_status().equals(GameDataRoom.GameStatus.not_started) &&
                        el.getValue().getPlayer_list().size() < 4
                        ).collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));

        List<RoomDto> roomDtoList = new ArrayList<>();
        for (Map.Entry<String, GameDataRoom> entry : playableRoomMap.entrySet()) {
            RoomDto roomDto = new RoomDto();
            roomDto.setId(entry.getKey());
            roomDto.setName(entry.getValue().getName());

            roomDtoList.add(roomDto);
        }

        return roomDtoList;
    }

    public GameDataRoom JoinRoom(String id, int playerId) {

        GameDataRoom room = gameRoomHash.entries(GAME_ROOM_HASH_KEY).get(id);

        // 1. validation
        Assert.isTrue(!room.getPlayer_list().contains(playerId), "Player already exist");

        // 2. add player to room's playerList
        room.getPlayer_list().add(playerId);

        // 3. add team or player
        Player player = playerRepository.findById(playerId).get();

        GameDataPlayer gameDataPlayer = new GameDataPlayer();
        gameDataPlayer.setId(player.getId());
        gameDataPlayer.setName(player.getName());

        Team playerTeam = player.getTeam();

        GameDataTeam gameDataTeam = new GameDataTeam();
        gameDataTeam.setId(playerTeam.getId());
        gameDataTeam.setName(playerTeam.getName());
        gameDataTeam.getPlayerList().add(gameDataPlayer);

        boolean teamExists = false;
        for(int i=0;i<room.getTeam_list().size();i++) {
            if (room.getTeam_list().get(i).getId() == gameDataTeam.getId()) {
                teamExists = true;
                break;
            }
        }

        if (teamExists) {
            // 1) add player if team already exist in the room
            room.getTeam_list().stream().filter(team -> team.getId() == gameDataTeam.getId()).findAny().get()
                    .getPlayerList().add(gameDataPlayer);
        }
        else {
            // 2) add team if team does not exist in the room
            room.getTeam_list().add(gameDataTeam);
        }

        gameRoomHash.put(GAME_ROOM_HASH_KEY, id, room);
        gameRoomUserHash.put(GAME_ROOM_USER_HASH_KEY, player.getId(), id);

        // 3. create redis channel
        ChannelTopic topic = gameRoomTopics.get(id);
        if (topic == null) {
            topic = new ChannelTopic(id);
            redisMessageListenerContainer.addMessageListener(onlineSubscriber, topic);
            gameRoomTopics.put(id, topic);
        }

        // 4. send message
        SendMessage(id);

        // 5. return room
        return room;
    }

    public GameDataRoom CreateRoom(String name, int quizId, int playerId) {

        // 1. Create new room
        GameDataRoom room = new GameDataRoom();

        // 1) Room name
        room.setName(name);
        room.getPlayer_list().add(playerId);

        // 2) Game quiz
        Quiz quiz = quizRepository.findById(quizId).get();
        room.getQuiz().setId(quiz.getId());
        room.getQuiz().setName(quiz.getName());
        room.getQuiz().setPlayerName(quiz.getPlayer().getName());

        quiz.getPairList().forEach(pair -> {

            GameDataPair pairA = new GameDataPair();

            pairA.setId(pair.getId());
            pairA.setType("A");
            pairA.setText(pair.getPairA());
            room.getQuiz().getPairList().add(pairA);

            GameDataPair pairB = new GameDataPair();

            pairB.setId(pair.getId());
            pairB.setType("B");
            pairB.setText(pair.getPairB());
            room.getQuiz().getPairList().add(pairB);
        });
        Collections.shuffle(room.getQuiz().getPairList());

        // 3) Game team
        Player player = playerRepository.findById(playerId).get();

        GameDataPlayer user = new GameDataPlayer();
        user.setId(player.getId());
        user.setName(player.getName());

        Team playerTeam = player.getTeam();

        GameDataTeam userTeam = new GameDataTeam();
        userTeam.setId(playerTeam.getId());
        userTeam.setName(playerTeam.getName());
        userTeam.getPlayerList().add(user);

        room.getTeam_list().add(userTeam);

        // 2. Add new room to gameRoomHash
        gameRoomHash.put(GAME_ROOM_HASH_KEY, room.getId(), room);
        gameRoomUserHash.put(GAME_ROOM_USER_HASH_KEY, player.getId(), room.getId());

        // 3. create redis channel
        ChannelTopic topic = new ChannelTopic(room.getId());
        redisMessageListenerContainer.addMessageListener(onlineSubscriber, topic);
        gameRoomTopics.put(room.getId(), topic);

        return room;
    }

    public void DeleteRoom(int playerId) {

        String roomId = gameRoomUserHash.get(GAME_ROOM_USER_HASH_KEY, playerId);

        if (roomId != null) {
            // 1. remove player from gameRoomUserHash
            gameRoomUserHash.delete(GAME_ROOM_USER_HASH_KEY, playerId);

            // 2. remove gameRoomHash if player does not exist in the room
            GameDataRoom roomHash = gameRoomHash.entries(GAME_ROOM_HASH_KEY).get(roomId);

            if (roomHash != null) {
                if (roomHash.getPlayer_list().contains(playerId)) {
                    roomHash.getPlayer_list().remove((Object)playerId);
                    gameRoomHash.put(GAME_ROOM_HASH_KEY, roomId, roomHash);
                }

                roomHash = gameRoomHash.entries(GAME_ROOM_HASH_KEY).get(roomId);
                if (roomHash.getPlayer_list().size() == 0)
                    gameRoomHash.delete(GAME_ROOM_HASH_KEY, roomId);
            }
        }
    }

    public void UpdateRoomGameData(GameDataRoom room) {
        switch (room.getGame_status()) {
            case not_started:
                gameRoomHash.put(GAME_ROOM_HASH_KEY, room.getId(), room);
                break;
            case started:
                gameRoomHash.put(GAME_ROOM_HASH_KEY, room.getId(), room);
                break;
            case finished:
                GameDataRoom roomHash = gameRoomHash.entries(GAME_ROOM_HASH_KEY).get(room.getId());
                if (roomHash != null) {
                    List<Integer> playerList = roomHash.getPlayer_list();
                    playerList.forEach(playerId -> {
                        gameRoomUserHash.delete(GAME_ROOM_USER_HASH_KEY, playerId);
                    });
                    gameRoomHash.delete(GAME_ROOM_HASH_KEY, room.getId());
                }
                break;
        }
    }

    public void SendMessage(String roomId) {

        onlinePublisher.Publish(gameRoomTopics.get(roomId), gameRoomHash.entries(GAME_ROOM_HASH_KEY).get(roomId));
    }
}