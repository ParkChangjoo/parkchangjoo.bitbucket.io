package com.pcj.project_nandemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcj.project_nandemo.model.dto.game.GameDataRoom;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OnlineSubscriber implements MessageListener {

    private final ObjectMapper objectMapper;
    private final RedisTemplate<String, Object> redisTemplate;
    private final SimpMessageSendingOperations messagingTemplate;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        try {
            String strMsg = redisTemplate.getStringSerializer().deserialize(message.getBody());
            GameDataRoom gameDataRoom = objectMapper.readValue(strMsg, GameDataRoom.class);
            messagingTemplate.convertAndSend("/sub/topic/game/online/room/" + gameDataRoom.getId(), gameDataRoom);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}