package com.pcj.project_nandemo.repository;


import com.pcj.project_nandemo.model.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
    Player findByName(String name);
    List<Player> findByTeamId(int teamId);
}