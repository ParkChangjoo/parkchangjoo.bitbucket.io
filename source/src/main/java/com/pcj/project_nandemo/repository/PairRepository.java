package com.pcj.project_nandemo.repository;

import com.pcj.project_nandemo.model.entity.Pair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PairRepository extends JpaRepository<Pair, Integer> {
    List<Pair> findByQuizId(int quizId);
    int deleteByQuizId(int quizId);
}