package com.pcj.project_nandemo.repository;

import com.pcj.project_nandemo.model.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Integer> {
    Team findByName(String name);
}