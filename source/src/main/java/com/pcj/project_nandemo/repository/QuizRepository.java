package com.pcj.project_nandemo.repository;

import com.pcj.project_nandemo.model.entity.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {
    List<Quiz> findByPlayerId(int playerId);
}