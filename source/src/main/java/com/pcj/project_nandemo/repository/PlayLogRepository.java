package com.pcj.project_nandemo.repository;

import com.pcj.project_nandemo.model.entity.PlayLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayLogRepository extends JpaRepository<PlayLog, Integer> {
}