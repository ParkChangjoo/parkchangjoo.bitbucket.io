﻿﻿# About project
マッチングするペアを見つけるパズルゲームです。  
自分が覚えたい科目でクイズを作成し、オンライン上で相手と対決する事が出来ます。  
学生さん同士に勉強したい内容で遊べられたらと思います。  
  
![picture](content/images/tutorial.png)  

- - -

### Source code
https://bitbucket.org/ParkChangjoo/parkchangjoo.bitbucket.io/src/master/
### Language
Java, Html5/Css, Javascript
### Framework
Spring Boot, Vue
### Database
Mysql, Redis
### Etc
WebSocket (Stomp), Redis (Session, pub/sub)
### Browser support
Chrome 85.0.4183.121
### How to run
> ソースコードをClone  
> project_nandemo_initialize_dump.sqlファイルを実行し、データベースを生成  
> project_nandemo_dbuser_create.sqlファイルを実行し、データベースユーザーを生成  
> redis database password設定  
> gradlew buildを実行し、jarファイルを生成  
> java -jar 生成されたファイル名.jar を実行  
> http://localhost:5000へ接続
### Sample game
以下のurlに接続しゲームをテストして見る事が出来ます。  
https://parkchangjoo.bitbucket.io/index.html  
> Bitbucket static websiteの利用によって、content security policy reportの表示やゲーム速度低下が発生します。  
> Clone後Localでテスト可能です。実行ファイルはindex.htmlです。
### Author
Park Changjoo

- - -

### 今後の更新予定
1. Stompにjwt認証適用
2. CSS 適用 (モバイル用)