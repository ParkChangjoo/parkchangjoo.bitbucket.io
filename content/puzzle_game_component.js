﻿// 1. vm load
const puzzle_game_component = {
    props: ["game_data"],
    template:`
    <div>
        <h4 class="notice">{{notice}}</h4>
        <div class="teams">
            <div class="player" v-for="team in this.game_data.teamList">
                <div>{{team.name}}</div>
                <div>Score: {{team.score}}</div>
                <div class="players">
                    <div v-for="player in team.playerList">
                        <div>{{player.chat}}</div>
                        <div><img :src="player.pic" width="100" height="100" /></div>
                        <div>{{player.name}} {{player.score}}</div>
                        <div><button @click="uSetReady(team.id, player.id)" :disabled="player.ready">Ready</button></div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="quiz">
            <button class="block btn btn-info btn-sm" v-for="block in game_data.quiz.pairList" name="block" :tId="block.tId" :pId="block.pId" :disabled="block.disabled" :style="{opacity: block.opacity}" @click="uAction(block.id, block.type, game_data.userTeamId, game_data.userId)">{{block.text}}</button>
        </div>
    </div>
    `,
    data() {
        return {
            notice: "Press ready to start",
            progress: "beforeStart"
        }
    },
    beforeMount() {
        this.progress = "beforeMount";

        // validation
        // ex) throw "iAddTeam(): id is not defined";
        this.game_data.quiz.pairList.forEach((pair) => {
            pair.disabled = true;
            pair.opacity = "1";
            pair.tId == undefined;
            pair.pId == undefined;
        });

        this.game_data.teamList.forEach((team, teamIndex) => {
            team.score = 0;
            team.playerList.forEach(player => {
                player.score = 0;
                if (teamIndex == 0)
                    player.ready = false;
                else
                    player.ready = true;
            })
        });
    },
    mounted() {
        this.progress = "mounted";

        // update parent progress
        this.$emit('progress_listner', "gameLoaded");
    },
    methods: {
        uSetReady(tId, pId) {
            this.game_data.teamList.find(t => t.id == tId).playerList.find(p => p.id == pId).ready = true;

            let notReadyPlayer = this.game_data.teamList.find(function(team) {
                return team.playerList.find(p => p.ready == false);
            });
            if (notReadyPlayer == undefined) {
                this.game_data.quiz.pairList.forEach(block => {
                    block.disabled = false;
                });
                this.notice = "Game started";
                this.progress = "started";
            }
        },
        uAction(blockId, blockType, tId, pId) {
            let selected = this.game_data.quiz.pairList.find(d => d.tId == tId && d.pId == pId);
            let current = this.game_data.quiz.pairList.find(p => p.id == blockId && p.type == blockType);

            if (current.disabled) {
                return;
            }
            else if (selected == undefined) {
                current.tId = tId;
                current.pId = pId;
                current.disabled = true;
                this.cUpdateChat(tId, pId, 0);
            }
            else if (selected.id == current.id) {
                current.disabled = true;
                selected.tId = undefined;
                selected.pId = undefined;
                selected.opacity = 0.5;
                current.opacity = 0.5;
                this.cUpdateScore(tId, pId, 1);
                this.cUpdateChat(tId, pId, 1);
            }
            else {
                selected.tId = undefined;
                selected.pId = undefined;
                selected.disabled = false;
                this.cUpdateChat(tId, pId, 0);
            }

            // check if game is finished
            let enabledBlock = this.game_data.quiz.pairList.find(x => x.disabled == false);
            if (this.isGameStarted && enabledBlock == undefined)
                this.progress ="finished";
        },
        cUpdateScore(tId, pId, scoreGet) {
            let team = this.game_data.teamList.find(t => t.id == tId);
            let player = team.playerList.find(p => p.id == pId);

            team.score += scoreGet;
            player.score += scoreGet;
        },
        cUpdateChat(tId, pId, type) {
            // update chat
            // type: 0:basic, 1:good, 2:lose, 3:win
            let words = [
                ["This one","May be"],
                ["I found","Oh yeah"],
                ["I lose","No way"],
                ["I win","Thank you"]
            ];

            let team = this.game_data.teamList.find(t => t.id == tId);
            let player = team.playerList.find(p => p.id == pId);

            let rndIdx = Math.round(Math.random());
            let chat = words[type][rndIdx];
            player.chat = chat;

            // update avatar face
            player.pic = player.statusPicList[type];
        },
        cfindWinner() {
            let teamIds = [];
            let topScore = 0;
            this.game_data.teamList.forEach((team) => {
                if (team.score > topScore)
                    topScore = team.score;
            });
            this.game_data.teamList.forEach((team) => {
                if (team.score == topScore)
                    teamIds.push(team.id);
            });

            if (teamIds.length > 1)
                return undefined;
            else
                return teamIds[0];
        },
        iSimulate() {
            this.game_data.teamList.filter(team => team.id != this.game_data.userTeamId).forEach((team) => {
                team.playerList.forEach((player) => {
                    let ivMin = 1500;
                    let ivMax = 2500;
                    let iv = Math.floor(Math.random() * (ivMax - ivMin + 1)) + ivMin;

                    let simulator = setInterval(function() {
                        if (this.isGameStarted) {
                            // 1) check if game is over. If it is over, clear interval.
                            if (this.isGameFinished) {
                                clearInterval(simulator);
                            }
                            else {
                                // 1) find buttons (not disabled, visible)
                                let blockList = this.game_data.quiz.pairList.filter(b => b.opacity == 1 && b.disabled == false);

                                // 2) push one of buttons
                                if (blockList.length > 0) {
                                    let rndIdx = Math.floor(Math.random() * (blockList.length - 1));
                                    let blockToPush = blockList[rndIdx];
                                    this.uAction(blockToPush.id, blockToPush.type, team.id, player.id);
                                }
                            }
                        }
                    }.bind(this), iv);
                });
            });
        }
    },
    watch: {
        progress(newVal, oldVal) {
            if (newVal == "finished") {
                let winner = this.cfindWinner();
                if (winner == undefined) {
                    this.game_data.teamList.forEach((t) => {
                        t.playerList.forEach((p) => {
                            this.cUpdateChat(t.id, p.id, 2);
                        });
                    });
                    this.notice = "draw";
                }
                else {
                    let winnerTeamName = "";
                    this.game_data.teamList.forEach((t) => {
                        if (t.id == winner) {
                            winnerTeamName = t.name;
                            t.playerList.forEach((p) => {
                                this.cUpdateChat(t.id, p.id, 3);
                            });
                        }
                        else {
                            t.playerList.forEach((p) => {
                                this.cUpdateChat(t.id, p.id, 2);
                            });
                        }
                    });
                    this.notice = winnerTeamName + " win";
                }

                // show game over screen for 2 sec -> update parent progress
                setTimeout(function() {
                    this.$emit('result_listner', winner == this.game_data.userTeamId)
                    this.$emit('progress_listner', "gameFinished");
                }.bind(this), 2000);
            }
        }
    },
    computed: {
        isGameStarted() {
            return this.progress == "started" || this.progress == "finished";
        },
        isGameFinished() {
            return this.progress == "finished";
        }
    }
};