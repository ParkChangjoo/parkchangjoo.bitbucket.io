var gameData_boss = {
  "quiz": {
    "id": 2,
    "name": "History",
    "playerName": "sensei",
    "pairList": [
      {
        "id": 18,
        "type": "B",
        "text": "1939"
      },
      {
        "id": 15,
        "type": "B",
        "text": "Two"
      },
      {
        "id": 19,
        "type": "A",
        "text": "In World War II, who wrote in her diary about hidi"
      },
      {
        "id": 21,
        "type": "B",
        "text": "Papyrus"
      },
      {
        "id": 16,
        "type": "A",
        "text": "What are the mummy's coffins placed inside"
      },
      {
        "id": 15,
        "type": "A",
        "text": "How many wives did Henry VIII have beheaded?"
      },
      {
        "id": 19,
        "type": "B",
        "text": "Anne Frank"
      },
      {
        "id": 21,
        "type": "A",
        "text": "What did the Egyptians use to make paper?"
      },
      {
        "id": 16,
        "type": "B",
        "text": "A sarcophagus"
      },
      {
        "id": 17,
        "type": "A",
        "text": "What are the Mayans famous for building?"
      },
      {
        "id": 18,
        "type": "A",
        "text": "In which year did World War II begin?"
      },
      {
        "id": 17,
        "type": "B",
        "text": "Pyramids"
      }
    ]
  },
  "teamList": [
    {
      "id": 1,
      "name": "Team A",
      "playerList": [
        {
          "id": 6,
          "name": "You",
          "chat": "Hello",
          "pic": "content/images/default_base.png",
          "statusPicList": [
            "content/images/default_base.png",
            "content/images/default_match.png",
            "content/images/default_lose.png",
            "content/images/default_win.png"
          ]
        }
      ]
    },
    {
      "id": 3,
      "name": "Team C",
      "playerList": [
        {
          "id": 3,
          "name": "Sensei",
          "chat": "Hello~",
          "pic": "content/images/sensei_base.png",
          "statusPicList": [
            "content/images/sensei_base.png",
            "content/images/sensei_match.png",
            "content/images/sensei_lose.png",
            "content/images/sensei_win.png"
          ]
        },
        {
          "id": 4,
          "name": "Sagat",
          "chat": "Hello!",
          "pic": "content/images/sagat_base.png",
          "statusPicList": [
            "content/images/sagat_base.png",
            "content/images/sagat_match.png",
            "content/images/sagat_lose.png",
            "content/images/sagat_win.png"
          ]
        }
      ]
    }
  ],
  "userId": 6,
  "userTeamId": 1
}