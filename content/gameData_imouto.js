var gameData_imouto = {
  "quiz": {
    "id": 1,
    "name": "What I like",
    "playerName": "imouto",
    "pairList": [
      {
        "id": 10,
        "type": "B",
        "text": "Strawberry"
      },
      {
        "id": 8,
        "type": "A",
        "text": "My favorite food"
      },
      {
        "id": 11,
        "type": "B",
        "text": "Shake shack milkshake"
      },
      {
        "id": 11,
        "type": "A",
        "text": "My favorite juice"
      },
      {
        "id": 12,
        "type": "A",
        "text": "My favorite singer"
      },
      {
        "id": 12,
        "type": "B",
        "text": "Sekai no owari"
      },
      {
        "id": 10,
        "type": "A",
        "text": "My favorite fruit"
      },
      {
        "id": 13,
        "type": "A",
        "text": "My birthday"
      },
      {
        "id": 14,
        "type": "A",
        "text": "Why I hate you"
      },
      {
        "id": 8,
        "type": "B",
        "text": "Rose pasta"
      },
      {
        "id": 14,
        "type": "B",
        "text": "I don't know"
      },
      {
        "id": 13,
        "type": "B",
        "text": "2005-10-25"
      }
    ]
  },
  "teamList": [
    {
      "id": 1,
      "name": "Team A",
      "playerList": [
        {
          "id": 6,
          "name": "You",
          "chat": "-_-",
          "pic": "content/images/default_base.png",
          "statusPicList": [
            "content/images/default_base.png",
            "content/images/default_match.png",
            "content/images/default_lose.png",
            "content/images/default_win.png"
          ]
        }
      ]
    },
    {
      "id": 2,
      "name": "Team B",
      "playerList": [
        {
          "id": 2,
          "name": "Imouto",
          "chat": "-_-#",
          "pic": "content/images/imouto_base.png",
          "statusPicList": [
            "content/images/imouto_base.png",
            "content/images/imouto_match.png",
            "content/images/imouto_lose.png",
            "content/images/imouto_win.png"
          ]
        }
      ]
    }
  ],
  "userId": 6,
  "userTeamId": 1
}